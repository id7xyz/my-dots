#Aliases

alias pwd='pwd && pwd | xclip -sel clipboard'

#fix obvious typo's
alias cd..='cd ..'
alias pdw="pwd"
alias unin="doas sisyphus uninstall"
alias install="doas sisyphus install"

### git
alias ga="git add ."
alias gp="git push"
alias gm="git commit"
alias sudo="doas"

alias v="nvim"
alias al="nvim $HOME/.config/zsh/aliases.txt"
alias lv="cd /smssd/Linux\ Videos/"
alias cp="cp -r"
alias rm="rm -r"
alias mytime="tty-clock -ct"
alias up="topgrade --disable git_repos --disable kakoune"
alias utar="tar -zxvf"
alias ls="exa -aG --color=always --icons --group-directories-first"
alias la="exa -a --color=always --icons --group-directories-first"
alias cp="cp -i"

alias bu="rsync.sh"
alias samba="doas smbd start"

alias sprint="termdown 20m && echo SPRINT DONE && paplay /usr/share/sounds/freedesktop/stereo/complete.oga"
alias sprint30="termdown 30m && echo SPRINT DONE && paplay /usr/share/sounds/freedesktop/stereo/complete.oga"

alias i3con="nvim .config/i3/config"
alias zsh="nvim $HOME/.config/zsh/.zshrc"
alias vimrc="nvim .config/nvim/init.lua"

alias sc="cd $HOME/Downloads/gitthings/scripts"
alias gt="cd $HOME/Downloads/gitthings"
alias repo="cd $HOME/myrepo"
alias pf="cd /smssd/Podcast\ Files/The\ Linux\ Cast/podcast-files"
alias ms="cd $HOME/Documents/Pages/unbuild-site"
alias jn="cd $HOME/Documents/Journal/2023"
alias fanfic="cd $HOME/media/FanFic"
